import React, { useState } from 'react';
import { Text, TextInput, View, StyleSheet,TouchableOpacity} from 'react-native';

const App = () => {
  const [text, setText] = useState('');
  const [balanced, setBalanced] = useState('');

  const onSubmit = (text) => {
    //Verificación de cadena vacía
    if (text=='') {
        return setBalanced("Balanceado")        
    }
    else{
      var pieces = text.toLowerCase().split("")      
      var validChars = "abcdefghiyjklmnñopqrstuvwxyz :()"
      //Verificación de caracteres válidos
      for(var i = 0; i < text.length; i++){
        let posicion = validChars.indexOf(pieces[i])
          if (posicion == -1){
            console.log("Caracter no válido")
            return setBalanced("Desbalanceado")
          }
          setBalanced("Balanceado")
      }  

      var stack = []
      var emoticon = 0
      //Verificación de paréntesis y emoticones
      for(var i = 0; i <= text.length; i++){
        if (pieces[i] =='(' || pieces[i] ==')' || pieces[i] ==':') {
          
          if (pieces[i]=="(") {
            stack.push(pieces[i])            
            if (emoticon> 1){               
              setBalanced("Balanceado")
              emoticon=emoticon-1
            }
          }

          if (pieces[i] ==':' && (pieces[i+1] =='(' || pieces[i+1] ==')')) {
            emoticon++
          }

          if (pieces[i]==")") {
            if (stack.length === 0 ) {   
              if (emoticon>0){               
                setBalanced("Balanceado")
                emoticon=emoticon-1
              }else{
                setBalanced("Desbalanceado")
              }
            }else{              
              stack.pop()
            }
          }
        }
        
        if (i==text.length){
            if (stack.length > 0){
              if(emoticon!=0 && emoticon >= stack.length) {
                return setBalanced("Balanceado")
              }
              return setBalanced("Desbalanceado")
            }else{
              return balanced
            }
          }
      }
             
    }
  }
  //Limpiar Input y text
  const onClear = () => {
      setBalanced("")
      setText ("")
  }
  return (
    <View style={styles.container}>
        <Text style={styles.txt}> ¿Mensaje de paréntesis balanceados?</Text>
        <View style={{ flexDirection:'row',marginTop:20, }}>
          <TextInput
            style={styles.txtInput}
            placeholder="Escriba su texto aquí"
            onChangeText={text => setText(text)}
            defaultValue={text}
          />
          <TouchableOpacity style={styles.button} onPress={() => onSubmit(text)}>
              <Text style={styles.submit}>Verificar</Text>
          </TouchableOpacity>        
        </View>
        <Text style={{padding: 10, fontSize: 14}}>
          {balanced ? balanced : ""}
        </Text>
        <TouchableOpacity style={styles.clear} onPress={() => onClear()}>
          <Text style={styles.submit}>Limpiar</Text>
        </TouchableOpacity>
    </View>
  );
}

export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent:'center'
  },
  txt:{
    textAlign:'center',
    fontSize:20,
    fontWeight:'bold',
  },
  txtInput:{
    flex:0.7,
    height: 45,
    paddingHorizontal:10,
    borderColor: 'gray', 
    borderWidth: 1,
    borderRadius:7,
  },
  button:{
    backgroundColor:'green',
    height:45,
    justifyContent:'center',
    flex:0.3,
    borderRadius:10,
    marginLeft:10
  },
  clear:{
    marginTop:20,
    backgroundColor:'gray',
    height:40,
    justifyContent:'center',
    borderRadius:10,
  },
  submit:{
    textAlign:'center',
    color:'white',
    fontSize:16,
  },
})